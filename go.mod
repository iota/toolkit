module gitea.com/iota/toolkit

go 1.16

require (
	github.com/antonfisher/nested-logrus-formatter v1.3.1
	github.com/elliotchance/pie v1.39.0
	github.com/emicklei/proto v1.9.1
	github.com/emicklei/proto-contrib v0.9.2
	github.com/golang/protobuf v1.5.2
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/jhump/protoreflect v1.10.1
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/net v0.0.0-20211216030914-fe4d6282115f // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20211208223120-3a66f561d7aa // indirect
	google.golang.org/grpc v1.43.0 // indirect
	google.golang.org/protobuf v1.27.1
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
