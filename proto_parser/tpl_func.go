package proto_parser

import "strings"

// Len 返回字符串长度
func Len(s string) int {
	return len([]rune(s))
}

// IsBodyEmpty 是否不需要关心文档返回
func IsBodyEmpty(s string) bool {
	if len(s) == 0 || s == "{}" {
		return true
	}
	return false
}

// NotBodyEmpty 是否文档为空
func NotBodyEmpty(s string) bool {
	if len(s) == 0 || s == "{}" {
		return false
	}
	return true
}

// IsOuterProjectErrorCode 是否是项目外部的错误码
func IsOuterProjectErrorCode(err string) bool {
	if strings.Contains(err, ".") {
		return true
	}
	return false
}
